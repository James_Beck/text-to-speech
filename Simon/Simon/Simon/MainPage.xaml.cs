﻿using Plugin.TextToSpeech;
using System;
using Xamarin.Forms;

namespace Simon
{
    public partial class MainPage : ContentPage
    {
        public MainPage()
        {
            InitializeComponent();
        }

        private void tapimg(object sender, EventArgs e)
        {
            CrossTextToSpeech.Current.Speak
            (
            queue: true,
            text: SpokenText.Text,
            pitch: (float)Pitch.Value,
            speakRate: (float)Speed.Value
            );
        }
    }
}
